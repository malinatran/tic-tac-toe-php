<?php
declare(strict_types=1);
require_once('Game.php');
require_once('GameOptions.php');

$gameOptions = new GameOptions();
$gameOptions->start();
$players = $gameOptions->getPlayers();
$game = new Game($players);
$game->run();
