<?php
declare(strict_types=1);
require_once('AIStrategy/MinimaxAIStrategy.php');
require_once('AIStrategy/RandomAIStrategy.php');
require_once('Exception/OptionInvalidException.php');
require_once('Player/ComputerPlayer.php');
require_once('Player/HumanPlayer.php');
require_once('UserInterface/Copy.php');
require_once('UserInterface/UserInterface.php');

class GameOptions {

  const AI = 'ComputerPlayer';
  const HUMAN = 'HumanPlayer';
  const GAME_LEVEL = 'promptForGameLevel';
  const GAME_TYPE = 'promptForGameType';
  const PLAYER_ORDER = 'promptForPlayerOrder';

  private $interface, $playerOne, $playerTwo, $strategy;

  public function __construct($interface = null) {
    $this->interface = $interface ?: new UserInterface();
  }

  public function start(): void {
    $this->interface->displayWelcomeMessage();
    $this->getGameOption(GameOptions::GAME_TYPE);
  }

  public function getPlayers(): array {
    return [$this->playerOne, $this->playerTwo];
  }

  private function getGameOption(string $prompt): void {
    try {
      $option = $this->interface->$prompt();
      $this->setGameOption($option, $prompt);
    } catch (OptionInvalidException $e) {
      $this->interface->display($e->getMessage());
      $this->getGameOption($prompt);
    }
  }

  private function setGameOption(string $option, string $prompt): void {
    switch ([$option, $prompt]) {

      case ['1', GameOptions::GAME_TYPE]:
        $this->getGameOption(GameOptions::GAME_LEVEL);
        $this->getGameOption(GameOptions::PLAYER_ORDER);
        break;

      case ['2', GameOptions::GAME_TYPE]:
        $this->initializePlayers(GameOptions::HUMAN, GameOptions::HUMAN);
        break;

      case ['1', GameOptions::GAME_LEVEL]:
        $this->initializeStrategy('RandomAIStrategy');
        break;

      case ['2', GameOptions::GAME_LEVEL]:
        $this->initializeStrategy('MinimaxAIStrategy');
        break;

      case ['1', GameOptions::PLAYER_ORDER]:
        $this->initializePlayers(GameOptions::HUMAN, GameOptions::AI);
        break;

      case ['2', GameOptions::PLAYER_ORDER]:
        $this->initializePlayers(GameOptions::AI, GameOptions::HUMAN);
        break;

      default:
        throw new OptionInvalidException();
      }
  }

  private function initializeStrategy(string $strategy): void {
    $this->strategy = new $strategy();
  }

  private function initializePlayers(string $playerOne, string $playerTwo): void {
    $this->playerOne = new $playerOne(Copy::MARKER_X, $this->getStrategy($playerOne));
    $this->playerTwo = new $playerTwo(Copy::MARKER_O, $this->getStrategy($playerTwo));
  }

  private function getStrategy(string $player): ?AIStrategy {
    return $player === GameOptions::AI ? $this->strategy : null;
  }
}
