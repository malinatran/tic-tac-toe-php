<?php
declare(strict_types=1);

class CellUnavailableException extends Exception {

  const MESSAGE = 'That position has already been selected!';

  public function __construct(string $message = null, int $code = 0, Exception $previous = null) {
    $message = CellUnavailableException::MESSAGE;

    parent::__construct($message, $code, $previous);
  }
}
