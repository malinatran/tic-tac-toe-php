<?php
declare(strict_types=1);

class OptionInvalidException extends Exception {

  const MESSAGE = 'You must enter 1 or 2!';

  public function __construct(string $message = null, int $code = 0, Exception $previous = null) {
    $message = OptionInvalidException::MESSAGE;

    parent::__construct($message, $code, $previous);
  }
}
