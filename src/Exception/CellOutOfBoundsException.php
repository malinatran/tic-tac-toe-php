<?php
declare(strict_types=1);

class CellOutOfBoundsException extends Exception {

  const MESSAGE = 'That number is way too big!';

  public function __construct(string $message = null, int $code = 0, Exception $previous = null) {
    $message = CellOutOfBoundsException::MESSAGE;

    parent::__construct($message, $code, $previous);
  }
}
