<?php
declare(strict_types=1);

class CellNonIntegerException extends Exception {

  const MESSAGE = 'That is not a valid number!';

  public function __construct(string $message = null, int $code = 0, Exception $previous = null) {
    $message = CellNonIntegerException::MESSAGE;

    parent::__construct($message, $code, $previous);
  }
}
