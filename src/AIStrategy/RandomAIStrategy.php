<?php
declare(strict_types=1);
require_once __DIR__. '/../Board/TicTacToeBoard.php';
require_once('AIStrategy.php');

class RandomAIStrategy implements AIStrategy {

  public function getStrategicMove(TicTacToeBoard $tttBoard, string $marker): int {
    $cells = $tttBoard->getGrid();
    return $this->getEmptyCell($cells);
  }

  public function getEmptyCell(array $cells): int {
    $indexAndCells = [];

    foreach ($cells as $index => $cell) {
      if (is_null($cell)) {
        $indexAndCells[$index] = $cell;
      }
    }

    return $this->selectRandomCell($indexAndCells);
  }

  private function selectRandomCell(array $indexAndCells): int {
    return empty($indexAndCells) ? -1 : array_rand($indexAndCells);
  }
}
