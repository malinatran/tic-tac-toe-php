<?php
declare(strict_types=1);
require_once __DIR__ . '/../Board/TicTacToeBoard.php';
require_once('AIStrategy.php');

class MinimaxAIStrategy implements AIStrategy {

  private $depth, $grid, $marker, $opponentMarker, $tttBoard;

  public function getStrategicMove(TicTacToeBoard $tttBoard, string $currentMarker): int {
    $this->tttBoard = $tttBoard;
    $this->marker = $currentMarker;
    $this->depth = 0;
    $this->grid = $tttBoard->getGrid();
    $this->opponentMarker = $this->tttBoard->getOpponentMarker($currentMarker);

    return $this->getMinimaxMove($this->grid, $this->depth, $currentMarker);
  }

  private function getMinimaxMove(array $grid, int $depth, string $currentMarker): int {
    $scoredMoves = $this->scoreMoves($grid, $depth, $currentMarker);
    return $this->getBestMove($currentMarker, $scoredMoves);
  }

  private function scoreMoves(array $grid, int $depth, string $currentMarker): array {
    $scores = [];
    $this->tttBoard->setGrid($grid);

    foreach ($this->tttBoard->getEmptyCells() as $cell) {
      $scores[$cell] = $this->scoreMove($cell, $depth, $currentMarker);
    }

    return $scores;
  }

  private function scoreMove($cell, int $depth, string $currentMarker): int {
    $updatedBoard = $this->tttBoard->markCell($cell, $currentMarker);
    $score = $this->getScore(
      $updatedBoard->getGrid(),
      $depth + 1,
      $this->tttBoard->getOpponentMarker($currentMarker)
    );
    $this->tttBoard->clearCell($cell);

    return $score;
  }

  private function getScore(array $grid, int $depth, string $currentMarker): int {
    $this->tttBoard->setGrid($grid);

    switch (true) {
      case ($this->tttBoard->isWin()):
        return $this->calculateWin($grid, $depth);

      case ($this->tttBoard->isTie()):
        return 0;

      default:
        $scores = $this->scoreMoves($grid, $depth, $currentMarker);
        return $this->getBestScore($currentMarker, $scores);
    }
  }

  private function getBestMove(string $currentMarker, array $scores): int {
    return $this->getBestMoveAndScore($currentMarker, $scores)[0];
  }

  private function getBestScore(string $currentMarker, array $scores): int {
    return $this->getBestMoveAndScore($currentMarker, $scores)[1];
  }

  private function getBestMoveAndScore(string $currentMarker, array $scores): array {
    $maxScore = max($scores);
    $minScore = min($scores);

    return ($currentMarker === $this->marker)
         ? [array_search($maxScore, $scores), $maxScore]
         : [array_search($minScore, $scores), $minScore];
  }

  private function calculateWin(array $grid, int $depth): int {
    $this->tttBoard->setGrid($grid);
    $winningMarker = $this->tttBoard->getWinningMarker();

    return ($winningMarker === $this->marker)
         ? 10 - $depth
         : $depth - 10;
  }
}
