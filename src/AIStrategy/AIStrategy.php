<?php
declare(strict_types=1);
require_once __DIR__ . '/../Board/TicTacToeBoard.php';

interface AIStrategy {

  public function getStrategicMove(TicTacToeBoard $tttBoard, string $marker);
}
