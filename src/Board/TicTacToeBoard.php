<?php
declare(strict_types=1);
require_once __DIR__ . '/../UserInterface/Copy.php';
require_once('Board.php');

final class TicTacToeBoard extends Board {

  public function getOpponentMarker(string $marker): string {
    return $marker === Copy::MARKER_X ? Copy::MARKER_O : Copy::MARKER_X;
  }

  public function isWin(): bool {
    return !empty($this->getWinningMarker());
  }

  public function isTie(): bool {
    return !$this->isWin() && $this->isFull();
  }

  public function getWinningMarker(): string {
    $rows = $this->getRows();
    $columns = $this->getColumns();
    $diagonals = $this->getDiagonals();

    switch (true) {
      case ($this->isFilledWithSameMarker($rows)):
        return $this->getUniqueMarker($rows);

      case ($this->isFilledWithSameMarker($columns)):
        return $this->getUniqueMarker($columns);

      case ($this->isFilledWithSameMarker($diagonals)):
        return $this->getUniqueMarker($diagonals);

      default: return '';
    }
  }

  private function getRows(): array {
    return array_chunk($this->getGrid(), $this->getSize());
  }

  private function getColumns(): array {
    $transposedGrid = [];
    $rows = $this->getRows();

    foreach ($rows as $i => $row) {
      foreach ($row as $j => $column) {
        $transposedGrid[$j][$i] = $column;
      }
    }

    return $transposedGrid;
  }

  private function getDiagonals(): array {
    return [$this->getForwardDiagonal(), $this->getBackwardDiagonal()];
  }

  private function getForwardDiagonal(): array {
    $forwardDiagonal = [];
    $rows = $this->getRows();

    foreach ($this->traverseGridByCell() as $index) {
      $forwardDiagonal []= $rows[$index][$this->getSize() - 1 - $index];
    }

    return $forwardDiagonal;
  }

  private function getBackwardDiagonal(): array {
    $backwardDiagonal = [];
    $rows = $this->getRows();

    foreach ($this->traverseGridByCell() as $index) {
      $backwardDiagonal []= $rows[$index][$index];
    }

    return $backwardDiagonal;
  }

  private function isFilledWithSameMarker(array $matrix): bool {
    foreach ($this->traverseMatrix($matrix, [$this, 'hasOneUniqueMarker']) as $hasOneUniqueMarker) {
      if ($hasOneUniqueMarker) return true;
    }

    return false;
  }

  private function getUniqueMarker(array $matrix): string {
    $callback = function($line) {
      return $this->hasOneUniqueMarker($line) ? $line[0] : '';
    };

    foreach ($this->traverseMatrix($matrix, $callback) as $marker) {
      if (!empty($marker)) return $marker;
    }

    return '';
  }

  private function traverseGridByCell(): Generator {
    for ($index = 0; $index < $this->getSize(); $index++) {
      yield $index;
    }
  }

  private function traverseMatrix(array $matrix, callable $callback): Generator {
    foreach ($matrix as $line) {
      yield $callback($line);
    }
  }

  private function hasOneUniqueMarker(array $line): bool {
    $uniqueMarkers = array_unique($line);
    return $this->hasOneElement($uniqueMarkers) && $this->isNotNull($uniqueMarkers[0]);
  }

  private function hasOneElement(array $line): bool {
    return count($line) === 1;
  }

  private function isNotNull(string $element = null): bool {
    return !is_null($element);
  }
}
