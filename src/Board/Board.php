<?php
declare(strict_types=1);
require_once __DIR__ . '/../Exception/CellOutOfBoundsException.php';
require_once __DIR__ . '/../Exception/CellUnavailableException.php';

class Board {

  private $grid, $size;

  public function __construct(int $size = null) {
    $this->size = $size ?: 3;
    $this->grid = array_fill(0, $this->size * $this->size, null);
  }

  public function getGrid(): array {
    return $this->grid;
  }

  public function getSize(): int {
    return $this->size;
  }

  public function setGrid(array $grid): self {
    $this->grid = $grid;
    return $this;
  }

  public function isEmpty(): bool {
    return $this->isGridFullOrEmpty(false);
  }

  public function isFull(): bool {
    return $this->isGridFullOrEmpty(true);
  }

  public function getEmptyCells(): array {
    $emptyCells = [];
    $callback = function($index) {
      return $this->isCellEmpty($index) ? $index : null;
    };

    foreach ($this->traverseGridByIndex($callback) as $index) {
      if (!is_null($index)) {
        $emptyCells []= $index;
      }
    }

    return $emptyCells;
  }

  public function clearCell(int $cell): self {
    $this->grid[$cell] = null;
    return $this;
  }

  public function markCell(int $cell, string $marker): self {
    if ($this->isOutOfBounds($cell)) {
      throw new CellOutOfBoundsException();
    }

    if (!$this->isCellEmpty($cell)) {
      throw new CellUnavailableException();
    }

    $this->grid[$cell] = $marker;
    return $this;
  }

  private function isGridFullOrEmpty(bool $value): bool {
    foreach ($this->traverseGridByIndex([$this, 'isCellEmpty']) as $isCellEmpty) {
      if ($isCellEmpty === $value) return false;
    }

    return true;
  }

  private function traverseGridByIndex(callable $callback): Generator {
    foreach ($this->getGrid() as $index => $cell) {
      yield $callback($index);
    }
  }

  private function isOutOfBounds(int $cell): bool {
    $numOfCells = $this->getSize() * $this->getSize();
    return $cell < 0 || $cell > ($numOfCells - 1);
  }

  private function isCellEmpty(int $cell): bool {
    return is_null($this->grid[$cell]);
  }
}
