<?php
declare(strict_types=1);
require_once __DIR__ . '/../AIStrategy/AIStrategy.php';
require_once __DIR__ . '/../Board/TicTacToeBoard.php';
require_once('Player.php');

final class ComputerPlayer implements Player {

  private $marker, $strategy, $tttBoard;

  public function __construct(
    string $marker,
    AIStrategy $strategy = null,
    TicTacToeBoard $tttBoard = null
  ) {
    $this->marker = $marker;
    $this->strategy = $strategy;
    $this->tttBoard = $tttBoard ?: new TicTacToeBoard();
  }

  public function getMarker(): string {
    return $this->marker;
  }

  public function getMove(array $grid): int {
    $this->tttBoard->setGrid($grid);
    return $this->strategy->getStrategicMove($this->tttBoard, $this->marker);
  }
}
