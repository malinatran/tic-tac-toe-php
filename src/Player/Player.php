<?php
declare(strict_types=1);

interface Player {

  public function getMarker();
  public function getMove(array $grid);
}
