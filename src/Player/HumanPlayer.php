<?php
declare(strict_types=1);
require_once __DIR__ . '/../Exception/CellNonIntegerException.php';
require_once __DIR__ . '/../UserInterface/CellValidator.php';
require_once __DIR__ . '/../UserInterface/UserInterface.php';
require_once('Player.php');

class HumanPlayer implements Player {

  private $interface, $marker, $validator;

  public function __construct(
    string $marker,
    UserInterface $interface = null,
    CellValidator $validator = null
  ) {
    $this->marker = $marker;
    $this->interface = $interface ?: new UserInterface();
    $this->validator = $validator ?: new CellValidator();
  }

  public function getMarker(): string {
    return $this->marker;
  }

  public function getMove(array $grid): int {
    $this->interface->displayGrid($grid);
    $input = $this->interface->promptForMove();

    if ($this->validator->isValidNumber($input)) {
      return intval($input);
    }

    throw new CellNonIntegerException();
  }
}
