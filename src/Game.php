<?php
declare(strict_types=1);
require_once('Board/TicTacToeBoard.php');
require_once('UserInterface/UserInterface.php');

class Game {

  private $currentPlayer, $interface, $playerOne, $playerTwo, $tttBoard;

  public function __construct(
    array $players,
    TicTacToeBoard $tttBoard = null,
    UserInterface $interface = null
  ) {
    $this->interface = $interface ?: new UserInterface();
    $this->playerOne = $players[0];
    $this->playerTwo = $players[1];
    $this->tttBoard = $tttBoard ?: new TicTacToeBoard();
    $this->currentPlayer = $this->playerOne;
  }

  public function run(): void {
    while (!$this->isGameOver()) {
      $this->makeMove();
    }

    $this->interface->displayGrid($this->tttBoard->getGrid());
    $this->interface->displayOutcome($this->determineOutcome());
  }

  public function makeMove(): void {
    $marker = $this->currentPlayer->getMarker();

    try {
      $move = $this->getPlayerMove();
      $this->tttBoard->markCell($move, $marker);
      $this->switchPlayer();
    } catch (Exception $e) {
      $this->interface->display($e->getMessage());
      $this->makeMove();
    }
  }

  public function getCurrentPlayer(): Player {
    return $this->currentPlayer;
  }

  public function getGrid(): array {
    return $this->tttBoard->getGrid();
  }

  private function getPlayerMove(): int {
    return $this->currentPlayer->getMove($this->getGrid());
  }

  private function switchPlayer(): self {
    $this->currentPlayer = ($this->currentPlayer === $this->playerOne)
                         ? $this->playerTwo
                         : $this->playerOne;
    return $this;
  }

  private function isGameOver(): bool {
    return $this->tttBoard->isWin() || $this->tttBoard->isTie();
  }

  private function determineOutcome(): string {
    return $this->tttBoard->isWin()
         ? $this->tttBoard->getWinningMarker()
         : 'No one';
  }
}
