<?php
declare(strict_types=1);

final class BoardFormatter {

  public static function toString(array $grid): string {
    $stringified = '';
    $size = intval(sqrt(count($grid)));

    foreach ($grid as $index => $cell) {
      $stringified .= BoardFormatter::addNewLine($size, $index) .
                      BoardFormatter::getCellValue($cell, $index);
    }

    return $stringified . PHP_EOL;
  }

  private static function addNewLine(int $size, int $index): string {
    return BoardFormatter::isLastCellInRow($size, $index)
         ? PHP_EOL
         : '';
  }

  private static function isLastCellInRow(int $size, int $index): bool {
    return $index % $size === 0;
  }

  private static function getCellValue(string $cell = null, int $index): string {
    return is_null($cell) ? "$index " : "$cell ";
  }
}
