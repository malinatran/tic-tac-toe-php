<?php
declare(strict_types=1);

final class Copy {

  const MARKER_O = 'O';
  const MARKER_X = 'X';
  const PROMPT_FOR_GAME_LEVEL =
    'Enter (1) for an extremely easy game or
      (2) for an unbeatable game.';
  const PROMPT_FOR_GAME_TYPE =
    'Enter (1) for a single-player game or
      (2) for a two-player game.';
  const PROMPT_FOR_MOVE = 'Enter your move.';
  const PROMPT_FOR_PLAYER_ORDER =
    'Enter (1) to go first or
      (2) for the AI to go first.';
  const MESSAGE_GAME_OVER = 'Game over!';
  const MESSAGE_HELLO = 'Hello! Welcome to tic-tac-toe.';
  const MESSAGE_PLAYER_ORDER_AI_FIRST = 'Computer goes first!';
  const MESSAGE_PLAYER_ORDER_HUMAN_FIRST = 'You go first!';
  const MESSAGE_STRATEGY_MINIMAX = 'This game is literally unbeatable.';
  const MESSAGE_STRATEGY_RANDOM = 'Cool! You have a 200% chance of winning.';
}
