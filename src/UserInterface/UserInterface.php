<?php
declare(strict_types=1);
require_once('BoardFormatter.php');
require_once('Copy.php');

class UserInterface {

  public function display(string $message): void {
    echo $message . PHP_EOL;
  }

  public function displayGrid(array $grid): void {
    $this->display(BoardFormatter::toString($grid));
  }

  public function displayOutcome(string $winner): void {
    $outcome = implode(' ', array(Copy::MESSAGE_GAME_OVER, $winner, 'wins.'));
    $this->display($outcome);
  }

  public function displayWelcomeMessage(): void {
    $this->display(Copy::MESSAGE_HELLO);
  }

  public function promptForGameLevel(): string {
    $this->display(Copy::PROMPT_FOR_GAME_LEVEL);
    return $this->getInput();
  }

  public function promptForGameType(): string {
    $this->display(Copy::PROMPT_FOR_GAME_TYPE);
    return $this->getInput();
  }

  public function promptForMove(): string {
    $this->display(COPY::PROMPT_FOR_MOVE);
    return $this->getInput();
  }

  public function promptForPlayerOrder(): string {
    $this->display(COPY::PROMPT_FOR_PLAYER_ORDER);
    return $this->getInput();
  }

  public function getInput(): string {
    return trim(readline('> '));
  }
}
