<?php
declare(strict_types=1);

class CellValidator {

  public function isValidNumber(string $value): bool {
    $number = floatval($value);

    return is_numeric($value)
        && !($this->startsWithZero($value) && $this->isSingleDigit($value))
        && $this->isPositiveNumber($number)
        && $this->isWholeNumber($number);
  }

  private function startsWithZero(string $value): bool {
    return $value[0] === '0';
  }

  private function isSingleDigit(string $value): bool {
    return strlen($value) > 1;
  }

  private function isPositiveNumber(float $number): bool {
    return $number >= 0;
  }

  private function isWholeNumber(float $number): bool {
    return $number === round($number, 0);
  }
}
