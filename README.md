## Tic-Tac-Toe | PHP
A command-line, single-player game of tic-tac-toe played on a 3x3 board. User must first select a game level (either easy or difficult) before proceeding. User is the first player to make a move. User’s marker is X and the computer’s marker is O.

### Objectives
- Write an unbeatable tic-tac-toe program in a language previously not used before.
- Demonstrate idiomatic use of chosen language while conveying personal design sense and coding style.
- Adhere to [SOLID principles](https://www.wikiwand.com/en/SOLID_(object-oriented_design)) and incorporate test-driven development.

### Installation
- Install [PHP 7.1](http://php.net/) and [PHPUnit 6.0](https://phpunit.de/).
- In your terminal:

```
$ git clone git@gitlab.com:malinatran/tic-tac-toe-php.git
$ cd tic-tac-toe-php
```

### Run
To play the game, run `./play` from the root directory.

### Tests
- To run all tests, run `phpunit tests`.
- To run package-level tests, specify the file path (e.g. `phpunit tests/Board`).
- To run individual tests, specify the file path (e.g. `phpunit tests/Board/BoardTest.php`).

### Codebase
To better understand the code and naming conventions, read this [Gist](https://gist.github.com/malinatran/9f392ac623e5e911f787ce649adb2cc7).
