<?php
declare(strict_types=1);
require_once __DIR__ . '/../../src/UserInterface/Copy.php';
require_once __DIR__ . '/../../src/UserInterface/UserInterface.php';
require_once __DIR__ . '/../Fixtures.php';

use PHPUnit\Framework\TestCase;

final class UserInterfaceTest extends TestCase {

  protected $interface;

  protected function setUp() {
    $this->interface = new UserInterface();
  }

  public function testDisplayPrintsMessageWithNewLine(): void {
    $message = 'I love tic-tac-toe.';
    $this->expectOutputString($message . "\n");
    $this->interface->display($message);
  }

  public function testDisplayGridPrintsFormattedEmptyBoard(): void {
    $this->expectOutputString("\n0 1 2 \n3 4 5 \n6 7 8 \n\n");
    $this->interface->displayGrid(Fixtures::getEmptyGrid());
  }

  public function testDisplayGridPrintsFormattedPartialBoard(): void {
    $this->expectOutputString("\n0 1 2 \n3 X 5 \n6 7 8 \n\n");
    $this->interface->displayGrid(Fixtures::getPartialGrid());
  }

  public function testDisplayGridPrintsFormattedFullBoard(): void {
    $this->expectOutputString("\nO O X \nX O O \nO X X \n\n");
    $this->interface->displayGrid(Fixtures::getFullGrid());
  }

  public function testDisplayOutcomePrintsWinningMarker(): void {
    $this->expectOutputString("Game over! Malina wins.\n");
    $this->interface->displayOutcome('Malina');
  }

  public function testDisplayWelcomeMessagePrintsMessageWithNewLine(): void {
    $this->expectOutputString(Copy::MESSAGE_HELLO . "\n");
    $this->interface->displayWelcomeMessage();
  }

  public function testPromptForGameLevelReturnsUserInput() {
    $stub = $this->getMockBuilder(UserInterface::class)
                 ->setMethods(array('getInput'))
                 ->getMock();
    $stub->method('getInput')
         ->willReturn('1');
    $this->expectOutputString(Copy::PROMPT_FOR_GAME_LEVEL . "\n");
    $this->assertEquals('1', $stub->promptForGameLevel());
  }

  public function testPromptForGameTypeReturnsUserInput() {
    $stub = $this->getMockBuilder(UserInterface::class)
                 ->setMethods(array('getInput'))
                 ->getMock();
    $stub->method('getInput')
         ->willReturn('2');
    $this->expectOutputString(Copy::PROMPT_FOR_GAME_LEVEL . "\n");
    $this->assertEquals('2', $stub->promptForGameLevel());
  }

  public function testPromptForMoveReturnsUserInput() {
    $stub = $this->getMockBuilder(UserInterface::class)
                 ->setMethods(array('getInput'))
                 ->getMock();
    $stub->method('getInput')
         ->willReturn('5');
    $this->assertEquals('5', $stub->promptForMove());
  }
}
