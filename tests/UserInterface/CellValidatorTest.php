<?php
declare(strict_types=1);
require_once __DIR__ . '/../../src/UserInterface/CellValidator.php';

use PHPUnit\Framework\TestCase;

final class CellValidatorTest extends TestCase {

  protected $validator;

  protected function setUp() {
    $this->validator = new CellValidator();
  }

  public function testIsValidNumberReturnsTrueIfNumberIsPositive(): void {
    $this->assertTrue($this->validator->isValidNumber('6'));
  }

  public function testIsValidNumberReturnsFalseIfNumberIsNegative(): void {
    $this->assertFalse($this->validator->isValidNumber('-3'));
  }

  public function testIsValidNumberReturnsFalseIfNumberIsFloat(): void {
    $this->assertFalse($this->validator->isValidNumber('23.5'));
  }

  public function testIsValidNumberReturnsFalseIfNumbericStringIsNotANumber(): void {
    $this->assertFalse($this->validator->isValidNumber('Hi, my name is Malina.'));
  }

  public function testIsValidNumberReturnsReturnsIfNumberIsPrecededByZero(): void {
    $this->assertFalse($this->validator->isValidNumber('01'));
  }
}
