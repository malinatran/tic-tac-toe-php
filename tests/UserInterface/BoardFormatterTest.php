<?php
declare(strict_types=1);
require_once __DIR__ . '/../../src/UserInterface/BoardFormatter.php';
require_once __DIR__ . '/../Fixtures.php';

use PHPUnit\Framework\TestCase;

final class BoardFormatterTest extends TestCase {

  public function testToStringConvertsArrayToStringAndConvertsNullsToNumbers(): void {
    $formattedBoard = "\n0 1 2 \n3 4 5 \n6 7 8 \n";
    $emptyGrid = Fixtures::getEmptyGrid();
    $this->assertEquals($formattedBoard, BoardFormatter::toString($emptyGrid));
  }

  public function testToStringConvertsNullsButNotMarkers(): void {
    $formattedBoard = "\n0 1 2 \n3 X 5 \n6 7 8 \n";
    $partialGrid = Fixtures::getPartialGrid();
    $this->assertEquals($formattedBoard, BoardFormatter::toString($partialGrid));
  }

  public function testToStringDoesNotConvertMarkers(): void {
    $formattedBoard = "\nO O X \nX O O \nO X X \n";
    $fullGrid = Fixtures::getFullGrid();
    $this->assertEquals($formattedBoard, BoardFormatter::toString($fullGrid));
  }
}
