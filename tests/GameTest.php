<?php
declare(strict_types=1);
require_once __DIR__ . '/../src/AIStrategy/MinimaxAIStrategy.php';
require_once __DIR__ . '/../src/Board/TicTacToeBoard.php';
require_once __DIR__ . '/../src/Game.php';
require_once __DIR__ . '/../src/Player/ComputerPlayer.php';
require_once __DIR__ . '/../src/Player/HumanPlayer.php';
require_once __DIR__ . '/../src/UserInterface/Copy.php';
require_once __DIR__ . '/Fixtures.php';

use PHPUnit\Framework\TestCase;

final class GameTest extends TestCase {

  protected $computerPlayer, $humanPlayer, $tttBoard;

  protected function setUp() {
    $this->computerPlayer = new ComputerPlayer(Copy::MARKER_O, new MinimaxAIStrategy());
    $this->humanPlayer = new HumanPlayer(Copy::MARKER_X);
    $this->tttBoard = new TicTacToeBoard();
  }

  public function testRunPrintsWinningMarkerIfGameIsOver(): void {
    $this->tttBoard->setGrid(Fixtures::getFilledRow());
    $game = new Game([$this->humanPlayer, $this->computerPlayer], $this->tttBoard);
    $stringifiedGrid = "\nO O X \nX X X \nO X O \n\n";
    $this->expectOutputString($stringifiedGrid . Copy::MESSAGE_GAME_OVER . " X wins.\n");
    $game->run();
  }

  public function testRunPrintsTieOutcomeIfGameIsOver(): void {
    $this->tttBoard->setGrid(Fixtures::getFullGrid());
    $game = new Game([$this->humanPlayer, $this->computerPlayer], $this->tttBoard);
    $stringifiedGrid = "\nO O X \nX O O \nO X X \n\n";
    $this->expectOutputString($stringifiedGrid . Copy::MESSAGE_GAME_OVER . " No one wins.\n");
    $game->run();
  }

  public function testMakeMoveMarksBoard(): void {
    $this->tttBoard->setGrid(Fixtures::getPartialGrid());
    $game = new Game([$this->computerPlayer, $this->humanPlayer], $this->tttBoard);
    $game->makeMove();
    $grid = ['O', null, null, null, 'X', null, null, null, null];
    $this->assertEquals($grid, $game->getGrid());
  }

  public function testMakeMoveSwitchesPlayerAfterSuccessfullyMarkingBoard(): void {
    $this->tttBoard->setGrid(Fixtures::getPartialGrid());
    $game = new Game([$this->computerPlayer, $this->humanPlayer], $this->tttBoard);
    $game->makeMove();
    $this->assertEquals('HumanPlayer', get_class($game->getCurrentPlayer()));
  }

  public function testGetCurrentPlayerReturnsFirstPlayerInArray(): void {
    $game = new Game([$this->humanPlayer, $this->computerPlayer]);
    $this->assertEquals('HumanPlayer', get_class($game->getCurrentPlayer()));
  }

  public function testGetGridReturnsCurrentGrid(): void {
    $game = new Game([$this->humanPlayer, $this->computerPlayer]);
    $this->assertEquals(Fixtures::getEmptyGrid(), $game->getGrid());
  }
}
