<?php
declare(strict_types=1);
require_once __DIR__ . '/../src/AIStrategy/RandomAIStrategy.php';
require_once __DIR__ . '/../src/Player/ComputerPlayer.php';
require_once __DIR__ . '/../src/Player/HumanPlayer.php';
require_once __DIR__ . '/../src/UserInterface/Copy.php';
require_once __DIR__ . '/../src/GameOptions.php';
require_once __DIR__ . '/Fixtures.php';

use PHPUnit\Framework\TestCase;

final class GameOptionsTest extends TestCase {

  public function testStartInvokesDisplayWelcomeMessage(): void {
    $stub = $this->createMock(UserInterface::class);
    $stub->expects($this->once())
         ->method('displayWelcomeMessage');
    $stub->method('promptForGameType')
         ->willReturn('2');
    $gameOptions = new GameOptions($stub);
    $gameOptions->start();
  }

  public function testStartInvokesPromptForGameTypeIfOptionIsInvalid(): void {
    $stub = $this->createMock(UserInterface::class);
    $stub->expects($this->exactly(2))
         ->method('promptForGameType')
         ->willReturnOnConsecutiveCalls('45', '2');
    $gameOptions = new GameOptions($stub);
    $gameOptions->start();
  }

  public function testStartInvokesPromptForGameLevelIfOptionIsInvalid(): void {
    $stub = $this->createMock(UserInterface::class);
    $stub->method('promptForGameType')
         ->willReturn('1');
    $stub->expects($this->exactly(2))
         ->method('promptForGameLevel')
         ->willReturnOnConsecutiveCalls('3', '1');
    $stub->method('promptForPlayerOrder')
         ->willReturn('1');
    $gameOptions = new GameOptions($stub);
    $gameOptions->start();
  }

  public function testStartInvokesPromptForPlayerOrderIfOptionIsInvalid(): void {
    $stub = $this->createMock(UserInterface::class);
    $stub->method('promptForGameType')
         ->willReturn('1');
    $stub->method('promptForGameLevel')
         ->willReturn('1');
    $stub->expects($this->exactly(3))
         ->method('promptForPlayerOrder')
         ->willReturnOnConsecutiveCalls('3', '6', '1');
    $gameOptions = new GameOptions($stub);
    $gameOptions->start();
  }

  public function testGetPlayersReturnsTwoHumanPlayers(): void {
    $playerOne = new HumanPlayer(Copy::MARKER_X);
    $playerTwo = new HumanPlayer(Copy::MARKER_O);
    $stub = $this->createMock(UserInterface::class);
    $stub->method('promptForGameType')
         ->willReturn('2');
    $gameOptions = new GameOptions($stub);
    $gameOptions->start();
    $this->assertEquals([$playerOne, $playerTwo], $gameOptions->getPlayers());
  }

  public function testGetPlayersReturnsPlayersWithHumanFirstAndRandomStrategy(): void {
    $playerOne = new HumanPlayer(Copy::MARKER_X);
    $playerTwo = new ComputerPlayer(Copy::MARKER_O, new RandomAIStrategy());
    $stub = $this->createMock(UserInterface::class);
    $stub->method('promptForGameType')
         ->willReturn('1');
    $stub->method('promptForGameLevel')
         ->willReturn('1');
    $stub->method('promptForPlayerOrder')
         ->willReturn('1');
    $gameOptions = new GameOptions($stub);
    $gameOptions->start();
    $this->assertEquals([$playerOne, $playerTwo], $gameOptions->getPlayers());
  }

  public function testGetPlayersReturnsPlayersWithHumanFirstAndMinimaxStrategy(): void {
    $playerOne = new HumanPlayer(Copy::MARKER_X);
    $playerTwo = new ComputerPlayer(Copy::MARKER_O, new MinimaxAIStrategy());
    $stub = $this->createMock(UserInterface::class);
    $stub->method('promptForGameType')
         ->willReturn('1');
    $stub->method('promptForGameLevel')
         ->willReturn('2');
    $stub->method('promptForPlayerOrder')
         ->willReturn('1');
    $gameOptions = new GameOptions($stub);
    $gameOptions->start();
    $this->assertEquals([$playerOne, $playerTwo], $gameOptions->getPlayers());
  }

  public function testGetPlayersReturnsPlayersWithAIFirstAndRandomStrategy(): void {
    $playerOne = new ComputerPlayer(Copy::MARKER_X, new RandomAIStrategy());
    $playerTwo = new HumanPlayer(Copy::MARKER_O);
    $stub = $this->createMock(UserInterface::class);
    $stub->method('promptForGameType')
         ->willReturn('1');
    $stub->method('promptForGameLevel')
         ->willReturn('1');
    $stub->method('promptForPlayerOrder')
         ->willReturn('2');
    $gameOptions = new GameOptions($stub);
    $gameOptions->start();
    $this->assertEquals([$playerOne, $playerTwo], $gameOptions->getPlayers());
  }

  public function testGetPlayersReturnsPlayersWithAIFirstAndMinimaxStrategy(): void {
    $playerOne = new ComputerPlayer(Copy::MARKER_X, new MinimaxAIStrategy());
    $playerTwo = new HumanPlayer(Copy::MARKER_O);
    $stub = $this->createMock(UserInterface::class);
    $stub->method('promptForGameType')
         ->willReturn('1');
    $stub->method('promptForGameLevel')
         ->willReturn('2');
    $stub->method('promptForPlayerOrder')
         ->willReturn('2');
    $gameOptions = new GameOptions($stub);
    $gameOptions->start();
    $this->assertEquals([$playerOne, $playerTwo], $gameOptions->getPlayers());
  }
}
