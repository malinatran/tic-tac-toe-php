<?php
declare(strict_types=1);
require_once __DIR__ . '/../../src/Exception/CellOutOfBoundsException.php';

use PHPUnit\Framework\TestCase;

final class CellOutOfBoundsExceptionTest extends TestCase {

  protected $exception;

  protected function setUp() {
    $this->exception = new CellOutOfBoundsException();
  }

  public function testConstructorReturnsCustomMessage(): void {
    $this->assertEquals(CellOutOfBoundsException::MESSAGE, $this->exception->getMessage());
  }
}
