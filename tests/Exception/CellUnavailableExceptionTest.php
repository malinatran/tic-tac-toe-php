<?php
declare(strict_types=1);
require_once __DIR__ . '/../../src/Exception/CellUnavailableException.php';

use PHPUnit\Framework\TestCase;

final class CellUnavailableExceptionTest extends TestCase {

  protected $exception;

  protected function setUp() {
    $this->exception = new CellUnavailableException();
  }

  public function testConstructorReturnsCustomMessage(): void {
    $this->assertEquals(CellUnavailableException::MESSAGE, $this->exception->getMessage());
  }
}
