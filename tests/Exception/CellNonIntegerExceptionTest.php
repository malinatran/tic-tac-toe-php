<?php
declare(strict_types=1);
require_once __DIR__ . '/../../src/Exception/CellNonIntegerException.php';

use PHPUnit\Framework\TestCase;

final class CellNonIntegerExceptionTest extends TestCase {

  protected $exception;

  protected function setUp() {
    $this->exception = new CellNonIntegerException();
  }

  public function testConstructorReturnsCustomMessage(): void {
    $this->assertEquals(CellNonIntegerException::MESSAGE, $this->exception->getMessage());
  }
}
