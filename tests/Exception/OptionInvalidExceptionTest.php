<?php
declare(strict_types=1);
require_once __DIR__ . '/../../src/Exception/OptionInvalidException.php';

use PHPUnit\Framework\TestCase;

final class OptionInvalidExceptionTest extends TestCase {

  protected $exception;

  protected function setUp() {
    $this->exception = new OptionInvalidException();
  }

  public function testConstructorReturnsCustomMessage(): void {
    $this->assertEquals(OptionInvalidException::MESSAGE, $this->exception->getMessage());
  }
}
