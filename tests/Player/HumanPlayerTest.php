<?php
declare(strict_types=1);
require_once __DIR__ . '/../../src/Exception/CellNonIntegerException.php';
require_once __DIR__ . '/../../src/Player/HumanPlayer.php';
require_once __DIR__ . '/../../src/UserInterface/CellValidator.php';
require_once __DIR__ . '/../../src/UserInterface/Copy.php';
require_once __DIR__ . '/../Fixtures.php';

use PHPUnit\Framework\TestCase;

final class HumanPlayerTest extends TestCase {

  protected $human, $validator;

  protected function setUp() {
    $this->human = new HumanPlayer(Copy::MARKER_O);
    $this->validator = new CellValidator();
  }

  public function testGetMarkerReturnsMarker(): void {
    $this->assertEquals(Copy::MARKER_O, $this->human->getMarker());
  }

  public function testGetMovePrintsGrid(): void {
    $stub = $this->getMockBuilder(UserInterface::class)
                 ->setMethods(array('promptForMove'))
                 ->getMock();
    $stub->method('promptForMove')
         ->willReturn('0');
    $human = $this->getHumanMock(Copy::MARKER_X, $stub, $this->validator);
    $this->expectOutputString("\n0 1 2 \n3 4 5 \n6 7 8 \n\n");
    $human->getMove(Fixtures::getEmptyGrid());
  }

  public function testGetMoveReturnsUserInputAsIntegerIfNumberIsValid(): void {
    $grid = Fixtures::getEmptyGrid();
    $stub = $this->createMock(HumanPlayer::class);
    $stub->method('getMove')
         ->willReturn('0');
    $this->assertEquals(0, $stub->getMove($grid));
  }

  public function testGetMoveThrowsNotValidExceptionIfNumberIsInvalid(): void {
    $stub = $this->createMock(UserInterface::class);
    $stub->method('promptForMove')
         ->willReturn('I am not a valid input.');
    $this->expectException(CellNonIntegerException::class);
    $this->expectExceptionMessage(CellNonIntegerException::MESSAGE);
    $human = $this->getHumanMock(Copy::MARKER_X, $stub, $this->validator);
    $human->getMove(Fixtures::getEmptyGrid());
  }

  private function getHumanMock($marker, $interface, $validator) {
    return new HumanPlayer($marker, $interface, $validator);
  }
}
