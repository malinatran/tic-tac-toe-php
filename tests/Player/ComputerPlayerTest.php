<?php
declare(strict_types=1);
require_once __DIR__ . '/../../src/AIStrategy/AIStrategy.php';
require_once __DIR__ . '/../../src/AIStrategy/MinimaxAIStrategy.php';
require_once __DIR__ . '/../../src/Player/ComputerPlayer.php';
require_once __DIR__ . '/../../src/UserInterface/Copy.php';
require_once __DIR__ . '/../Fixtures.php';

use PHPUnit\Framework\TestCase;

final class ComputerPlayerTest extends TestCase {

  protected $computer;

  protected function setUp() {
    $this->computer = new ComputerPlayer(Copy::MARKER_X);
  }

  public function testGetMarkerReturnsMarker(): void {
    $this->assertEquals(Copy::MARKER_X, $this->computer->getMarker());
  }

  public function testGetMoveReturnsValueFromAIStrategy(): void {
    $stub = $this->createMock(MinimaxAIStrategy::class);
    $stub->method('getStrategicMove')
         ->willReturn(2);
    $computer = $this->getComputerMock(Copy::MARKER_X, $stub, new TicTacToeBoard());
    $this->assertEquals(2, $computer->getMove(Fixtures::getGridWithOneEmptyCell()));
  }

  private function getComputerMock($marker, $minimax, $tttBoard) {
    return new ComputerPlayer($marker, $minimax, $tttBoard);
  }
}
