<?php
declare(strict_types=1);
require_once __DIR__ . '/../../src/Board/TicTacToeBoard.php';
require_once __DIR__ . '/../../src/UserInterface/Copy.php';
require_once __DIR__ . '/../Fixtures.php';

use PHPUnit\Framework\TestCase;

final class TicTacToeBoardTest extends TestCase {

  protected $tttBoard;

  protected function setUp() {
    $this->tttBoard = new TicTacToeBoard();
  }

  public function testGetOpponentMarkerReturnsOtherPlayerMarker(): void {
    $this->tttBoard->setGrid(Fixtures::getGridWithOneEmptyCell());
    $this->assertEquals(Copy::MARKER_O, $this->tttBoard->getOpponentMarker(Copy::MARKER_X));
  }

  public function testIsWinReturnsTrueIfRowIsFilledWithSameMarker(): void {
    $this->tttBoard->setGrid(Fixtures::getFilledRow());
    $this->assertTrue($this->tttBoard->isWin());
  }

  public function testIsWinReturnsFalseIfRowIsNotFilled(): void {
    $this->tttBoard->setGrid(Fixtures::getFullGrid());
    $this->assertFalse($this->tttBoard->isWin());
  }

  public function testIsWinReturnsTrueIfColumnIsFilledWithSameMarker(): void {
    $this->tttBoard->setGrid(Fixtures::getFilledColumn());
    $this->assertTrue($this->tttBoard->isWin());
  }

  public function testIsWinReturnsFalseIfColumnIsNotFilled(): void {
    $this->tttBoard->setGrid(Fixtures::getFullGrid());
    $this->assertFalse($this->tttBoard->isWin());
  }

  public function testIsWinReturnsTrueIfDiagonalIsFilledWithSameMarker(): void {
    $this->tttBoard->setGrid(Fixtures::getFilledDiagonal());
    $this->assertTrue($this->tttBoard->isWin());
  }

  public function testIsWinReturnsFalseIfNotFilled(): void {
    $this->tttBoard->setGrid(Fixtures::getFullGrid());
    $this->assertFalse($this->tttBoard->isWin());
  }

  public function testIsTieReturnsTrueIfThereIsNotAWinAndBoardIsFull(): void {
    $this->tttBoard->setGrid(Fixtures::getFullGrid());
    $this->assertTrue($this->tttBoard->isTie());
  }

  public function testIsTieReturnsFalseIfThereIsAWin(): void {
    $this->tttBoard->setGrid(Fixtures::getFilledColumn());
    $this->assertFalse($this->tttBoard->isTie());
  }

  public function testIsTieReturnsFalseIfBoardIsNotFull(): void {
    $this->tttBoard->setGrid(Fixtures::getPartialGrid());
    $this->assertFalse($this->tttBoard->isTie());
  }

  public function testGetWinningMarkerReturnsMarkerForFilledRow(): void {
    $this->tttBoard->setGrid(Fixtures::getFilledRow());
    $this->assertEquals(Copy::MARKER_X, $this->tttBoard->getWinningMarker());
  }

  public function testGetWinningMarkerReturnsMarkerForFilledColumn(): void {
    $this->tttBoard->setGrid(Fixtures::getFilledColumn());
    $this->assertEquals(Copy::MARKER_O, $this->tttBoard->getWinningMarker());
  }

  public function testGetWinningMarkerReturnsMarkerForFilledDiagonal(): void {
    $this->tttBoard->setGrid(Fixtures::getFilledDiagonal());
    $this->assertEquals(Copy::MARKER_X, $this->tttBoard->getWinningMarker());
  }

  public function testGetWinningMarkerReturnsEmptyStringIfThereIsNoWin(): void {
    $this->tttBoard->setGrid(Fixtures::getFullGrid());
    $this->assertEquals('', $this->tttBoard->getWinningMarker());
  }
}
