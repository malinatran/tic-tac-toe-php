<?php
declare(strict_types=1);
require_once __DIR__ . '/../../src/Board/Board.php';
require_once __DIR__ . '/../../src/Exception/CellOutOfBoundsException.php';
require_once __DIR__ . '/../../src/Exception/CellUnavailableException.php';
require_once __DIR__ . '/../../src/UserInterface/Copy.php';
require_once __DIR__ . '/../Fixtures.php';

use PHPUnit\Framework\TestCase;

final class BoardTest extends TestCase {

  protected $board;

  protected function setUp() {
    $this->board = new Board();
  }

  public function testBoardConstructorSetsCustomSize(): void {
    $testBoard = new Board(4);
    $this->assertEquals(4, $testBoard->getSize());
  }

  public function testGetGridReturnsGridAsArray(): void {
    $this->assertEquals(Fixtures::getEmptyGrid(), $this->board->getGrid());
  }

  public function testGetSizeReturnsSizeOfBoard(): void {
    $this->assertEquals(3, $this->board->getSize());
  }

  public function testIsEmptyReturnsTrueIfBoardIsEmpty(): void {
    $this->assertTrue($this->board->isEmpty());
  }

  public function testIsEmptyReturnsFalseIfBoardIsPartiallyFilled(): void {
    $this->board->setGrid(Fixtures::getPartialGrid());
    $this->assertFalse($this->board->isEmpty());
  }

  public function testIsEmptyReturnsFalseIfBoardIsFull(): void {
    $this->board->setGrid(Fixtures::getFullGrid());
    $this->assertFalse($this->board->isEmpty());
  }

  public function testIsFullReturnsTrueIfBoardIsFull(): void {
    $this->board->setGrid(Fixtures::getFullGrid());
    $this->assertTrue($this->board->isFull());
  }

  public function testIsFullReturnsFalseIfBoardIsPartiallyFilled(): void {
    $this->board->setGrid(Fixtures::getPartialGrid());
    $this->assertFalse($this->board->isFull());
  }

  public function testIsFullReturnsFalseIfBoardIsEmpty(): void {
    $this->board->setGrid(Fixtures::getEmptyGrid());
    $this->assertFalse($this->board->isFull());
  }

  public function testGetEmptyCellsReturnsArrayOfIndicesIfGridIsEmpty(): void {
    $this->board->setGrid(Fixtures::getPartialGrid());
    $this->assertEquals(Fixtures::getEmptyCells(), $this->board->getEmptyCells());
  }

  public function testGetEmptyCellsReturnsEmptyArrayIfGridIsFull(): void {
    $this->board->setGrid(Fixtures::getFullGrid());
    $this->assertEquals([], $this->board->getEmptyCells());
  }

  public function testClearCellReturnsBoardWithPreviouslyMarkedCellRemoved(): void {
    $this->board->setGrid(Fixtures::getPartialGrid());
    $this->assertEquals(Fixtures::getEmptyGrid(), $this->board->clearCell(4)->getGrid());
  }

  public function testMarkCellReturnsMarkedBoardIfCellIsValidAndEmpty(): void {
    $this->board->setGrid(Fixtures::getEmptyGrid());
    $this->assertEquals(Fixtures::getPartialGrid(), $this->board->markCell(4, Copy::MARKER_X)->getGrid());
  }

  public function testMarkCellThrowsNotAvailableExceptionIfCellIsNotEmpty(): void {
    $this->board->setGrid(Fixtures::getPartialGrid());
    $this->expectException(CellUnavailableException::class);
    $this->expectExceptionMessage(CellUnavailableException::MESSAGE);
    $this->board->markCell(4, Copy::MARKER_O);
  }

  public function testMarkCellThrowsOutOfBoundsExceptionIfGreaterThanSize(): void {
    $this->expectException(CellOutOfBoundsException::class);
    $this->expectExceptionMessage(CellOutOfBoundsException::MESSAGE);
    $this->board->markCell(20, Copy::MARKER_X);
  }

  public function testMarkCellsThrowsOutOfBoundsExceptionIfEqualToGridLength(): void {
    $this->expectException(CellOutOfBoundsException::class);
    $this->expectExceptionMessage(CellOutOfBoundsException::MESSAGE);
    $this->board->markCell(9, Copy::MARKER_X);
  }

  public function testMarkCellsThrowsOutOfBoundsExceptionIfLessThanZero(): void {
    $this->expectException(CellOutOfBoundsException::class);
    $this->expectExceptionMessage(CellOutOfBoundsException::MESSAGE);
    $this->board->markCell(-10, Copy::MARKER_X);
  }
}
