<?php

final class Fixtures {

  public static function getEmptyGrid() {
    return [null, null, null,
            null, null, null,
            null, null, null];
  }

  public static function getPartialGrid() {
    return [null, null, null,
            null, 'X', null,
            null, null, null];
  }

  public static function getFullGrid() {
    return ['O', 'O', 'X',
            'X', 'O', 'O',
            'O', 'X', 'X'];
  }

  public static function getGridWithTwoEmptyCells() {
    return ['X', 'O', 'O',
            'O', 'X', 'X',
            null, null, 'O'];
  }

  public static function getGridWithOneEmptyCell() {
    return ['O', 'X', 'X',
            'O', 'X', 'X',
            null, 'X', 'O'];
  }

  public static function getAlmostWinningGrid() {
    return [null, 'X', 'O',
            'O', 'X', null,
            null, null, null];
  }

  public static function getFilledRow() {
    return ['O', 'O', 'X',
            'X', 'X', 'X',
            'O', 'X', 'O'];
  }

  public static function getFilledColumn() {
    return ['X', 'O', 'X',
            'X', 'O', 'X',
            'O', 'O', 'O'];
  }

  public static function getFilledDiagonal() {
    return ['X', 'O', 'X',
            'O', 'X', 'X',
            'X', 'O', 'O'];
  }

  public static function getEmptyCells() {
    return [0, 1, 2, 3, 5, 6, 7, 8];
  }
}
