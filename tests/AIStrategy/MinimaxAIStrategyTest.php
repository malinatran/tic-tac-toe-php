<?php
declare(strict_types=1);
require_once __DIR__ . '/../../src/AIStrategy/MinimaxAIStrategy.php';
require_once __DIR__ . '/../../src/Board/TicTacToeBoard.php';
require_once __DIR__ . '/../../src/UserInterface/Copy.php';
require_once __DIR__ . '/../Fixtures.php';

use PHPUnit\Framework\TestCase;

class MinimaxAIStrategyTest extends TestCase {

  protected $computerMarker, $minimax, $tttBoard;

  protected function setUp() {
    $this->tttBoard = new TicTacToeBoard();
    $this->minimax = new MinimaxAIStrategy();
    $this->computerMarker = Copy::MARKER_X;
  }

  public function testGetStrategicMoveReturnsLeftCornerMoveAsFirstMove(): void {
    $this->tttBoard->setGrid(Fixtures::getEmptyGrid());
    $this->assertEquals(0, $this->minimax->getStrategicMove($this->tttBoard, $this->computerMarker));
  }

  public function testGetStrategicMoveReturnsWinningMove(): void {
    $this->tttBoard->setGrid(Fixtures::getAlmostWinningGrid());
    $this->assertEquals(7, $this->minimax->getStrategicMove($this->tttBoard, $this->computerMarker));
  }

  public function testGetStrategicMoveReturnsBlockingMove(): void {
    $this->tttBoard->setGrid(Fixtures::getAlmostWinningGrid());
    $this->assertEquals(7, $this->minimax->getStrategicMove($this->tttBoard, Copy::MARKER_O));
  }

  public function testGetStrategicMoveReturnsLowerNumberCellIfGameWillEventuallyTie(): void {
    $this->tttBoard->setGrid(Fixtures::getGridWithTwoEmptyCells());
    $this->assertEquals(6, $this->minimax->getStrategicMove($this->tttBoard, $this->computerMarker));
  }

  public function testGetStrategicMoveReturnsOnlyAvailableMove(): void {
    $this->tttBoard->setGrid(Fixtures::getGridWithOneEmptyCell());
    $this->assertEquals(6, $this->minimax->getStrategicMove($this->tttBoard, $this->computerMarker));
  }
}
