<?php
declare(strict_types=1);
require_once __DIR__ . '/../../src/AIStrategy/RandomAIStrategy.php';
require_once __DIR__ . '/../../src/Board/TicTacToeBoard.php';
require_once __DIR__ . '/../../src/UserInterface/Copy.php';
require_once __DIR__ . '/../Fixtures.php';

use PHPUnit\Framework\TestCase;

final class RandomAIStrategyTest extends TestCase {

  protected $random, $tttBoard;

  protected function setUp() {
    $this->tttBoard = new TicTacToeBoard();
    $this->random = new RandomAIStrategy();
  }

  public function testGetStrategicMoveReturnsOnlyCellAvailable(): void {
    $this->tttBoard->setGrid(Fixtures::getGridWithOneEmptyCell());
    $this->assertEquals(6, $this->random->getStrategicMove($this->tttBoard, Copy::MARKER_X));
  }

  public function testGetStrategicMoveReturnsNegative1IfNoEmptyCell(): void {
    $this->tttBoard->setGrid(Fixtures::getFullGrid());
    $this->assertEquals(-1, $this->random->getStrategicMove($this->tttBoard, Copy::MARKER_X));
  }

  public function testGetStrategicMoveInvokesGetRandomIndex(): void {
    $this->tttBoard->setGrid(Fixtures::getEmptyGrid());
    $stub = $this->getMockBuilder(RandomAIStrategy::class)
                 ->setMethods(array('getEmptyCell'))
                 ->getMock();
    $stub->method('getEmptyCell')
         ->willReturn(5);
    $this->assertEquals(5, $stub->getStrategicMove($this->tttBoard, Copy::MARKER_X));
  }
}
